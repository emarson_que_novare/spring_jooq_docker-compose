FROM openjdk:8-jdk-alpine
ADD spring-jooq.jar spring-jooq.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "spring-jooq.jar" ]